const { Router } = require('express')

const accessMiddleware = require('../../utils/accessMiddleware')
const validate = require('../../utils/validationMiddleware')

const AccountCtrl = require('./controllers/AccountCtrl')
const AssetCtrl = require('./controllers/AssetCtrl')
const TransferCtrl = require('./controllers/TransferCtrl')
const TransactionCtrl = require('./controllers/TransactionCtrl')
const BalanceCtrl = require('./controllers/BalanceCtrl')
const BankAccountCtrl = require('./controllers/BankAccountCtrl')
const WalletAccountCtrl = require('./controllers/WalletAccountCtrl')

const AccountSchema = require('./validation/AccountSchema')
const AssetSchema = require('./validation/AssetSchema')
const TransferSchema = require('./validation/TransferSchema')
const TransactionSchema = require('./validation/TransactionSchema')
const BankAccountSchema = require('./validation/BankAccountSchema')
const WalletAccountSchema = require('./validation/WalletAccountSchema')

function CustodyRouter () {
  this.router = Router({ mergeParams: true })
  this.router.use(accessMiddleware)
  this.buildRoutes()
  return this.router
}

CustodyRouter.prototype.buildRoutes = async function () {
  try {
    const assetCtrl = new AssetCtrl()
    const transferCtrl = new TransferCtrl()
    const transactionCtrl = new TransactionCtrl()
    const accountCtrl = new AccountCtrl()
    const balanceCtrl = new BalanceCtrl()
    const bankAccountCtrl = new BankAccountCtrl()
    const walletAccountCtrl = new WalletAccountCtrl()

    this.router
      .post('/account', validate(AccountSchema), accountCtrl.post)
      .get('/account/:accountId?', accountCtrl.get)

    this.router
      .post('/bank-account', validate(BankAccountSchema), bankAccountCtrl.post)
      .put(
        '/bank-account/:bankAccountId',
        validate(BankAccountSchema),
        bankAccountCtrl.put
      )
      .get('/bank-account/:bankAccountId?', bankAccountCtrl.get)

    this.router
      .post(
        '/wallet-account',
        validate(WalletAccountSchema),
        walletAccountCtrl.post
      )
      .get('/wallet-account/:walletAccountId?', walletAccountCtrl.get)

    this.router
      .post('/asset', validate(AssetSchema), assetCtrl.post)
      .get('/asset/:assetId?', assetCtrl.get)
      .put('/asset/:assetId', validate(AssetSchema), assetCtrl.put)

    this.router.get('/balance/:accountId', balanceCtrl.get)

    this.router
      .post('/transfer', validate(TransferSchema), transferCtrl.post)
      .get('/transfer/:transferId?', transferCtrl.get)
      .put('/transfer/:transferId?', transferCtrl.put)
      .put('/transfer/:transferId', validate(TransferSchema), transferCtrl.put)
      .delete('/transfer/:transferId', transferCtrl.del)

    this.router
      .post('/transaction', validate(TransactionSchema), transactionCtrl.post)
      .get('/transaction/:transactionId?', transactionCtrl.get)
  } catch (err) {
    console.log(err)
  }
}

module.exports = CustodyRouter
