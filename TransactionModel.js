const mongoose = require('mongoose')

const TransactionSchema = mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    },
    assetId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Asset',
      required: [true, 'Asset Id is required.']
    },
    accountId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Account',
      required: [true, 'Account Id is required.']
    },
    type: {
      type: String,
      enum: ['CREDIT', 'DEBIT'],
      required: [true, 'Transaction type is required.']
    },
    context: {
      type: String,
      enum: ['TRANSFER', 'INVEST', 'TRADE', 'MINT'],
      required: [true, 'Transaction context is required.']
    },
    amount: {
      type: Number,
      required: [true, 'Amount is required.']
    },

    // TODO add pre hook to validate one of the following
    // is required.
    tradeId: {
      type: mongoose.Types.ObjectId,
      ref: 'Trade'
    },
    investmentId: {
      type: mongoose.Types.ObjectId,
      ref: 'Investment'
    },
    transferId: {
      type: mongoose.Types.ObjectId,
      ref: 'Transfer'
    }
  },
  { timestamps: true }
)

TransactionSchema.pre('update', () => {
  throw new Error('cannot update')
})
TransactionSchema.pre('delete', () => {
  throw new Error('cannot delete')
})

module.exports = mongoose.model('Transaction', TransactionSchema)
