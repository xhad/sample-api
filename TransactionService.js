const AccountModel = require('../models/AccountModel')
const TransactionModel = require('../models/TransactionModel')
const { Types } = require('mongoose')

function TransactionService () {}

TransactionService.prototype.createAny = async function ({
  accountId,
  type,
  amount,
  context
}) {
  try {
    const account = await AccountModel.findOne({
      _id: Types.ObjectId(accountId)
    })

    if (account) {
      const NewTransaction = new TransactionModel({
        accountId,
        assetId: account.assetId,
        type,
        amount,
        context
      })

      const result = await NewTransaction.save()
      return result
    } else {
      return false
    }
  } catch (err) {
    console.log(err)
    return false
  }
}

TransactionService.prototype.getAny = async function (
  transactionId,
  skip,
  lim
) {
  try {
    if (!transactionId) {
      const result = await TransactionModel.aggregate([
        {
          $facet: {
            stage1: [{ $group: { _id: null, count: { $sum: 1 } } }],
            stage2: [{ $skip: skip }, { $limit: lim }]
          }
        },
        { $unwind: '$stage1' },
        {
          $project: {
            list: '$stage2',
            total: '$stage1.count'
          }
        },
        { $addFields: { limit: lim, offset: skip } }
      ])
      return result[0]
    } else {
      return await TransactionModel.findOne({
        _id: Types.ObjectId(transactionId)
      })
        .populate('accountId')
        .populate('assetId')
    }
  } catch (err) {
    console.log(err)
    return false
  }
}

module.exports = TransactionService
