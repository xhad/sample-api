const TransactionService = require('../services/TransactionService')

const AccessControl = require('accesscontrol')

const { TRANSACTION_ACL } = require('../custody-acl')
const CustodyErrors = require('../CustodyErrors')

function TransactionCtrl () {}

TransactionCtrl.prototype.post = async function (req, res) {
  try {
    const ac = new AccessControl(TRANSACTION_ACL)
    const createAny = await ac.can(req.user.roles).createAny('transaction')
    const transactionService = new TransactionService()
    if (createAny.granted) {
      const result = await transactionService.createAny(req.body)

      result
        ? res.status(200).send({
            message: 'Success',
            data: result
          })
        : res.status(400).send(CustodyErrors.FAILED)
    } else {
      res.status(401).send(CustodyErrors.UNAUTHORIZED)
    }
  } catch (err) {
    console.log(err)
    res.status(500).send(CustodyErrors.FAILED)
  }
}

TransactionCtrl.prototype.get = async function (req, res) {
  try {
    const ac = new AccessControl(TRANSACTION_ACL)
    const createAny = await ac.can(req.user.roles).createAny('transaction')
    const transactionService = new TransactionService()
    const { transactionId } = req.params
    const { limit, offset } = req.query
    const lim = limit ? Number(limit) : 10
    const skip = offset ? Number(offset) : 0

    if (createAny.granted) {
      const result = await transactionService.getAny(transactionId, skip, lim)

      result
        ? res.status(200).send({
            message: 'Success',
            data: result
          })
        : res.status(400).send(CustodyErrors.FAILED)
    } else {
      res.status(401).send(CustodyErrors.UNAUTHORIZED)
    }
  } catch (err) {
    console.log(err)
    res.status(500).send(CustodyErrors.FAILED)
  }
}
module.exports = TransactionCtrl
