const express = require('express')
const logger = require('morgan')
const bodyParser = require('body-parser')
const helmet = require('helmet')
const fileUpload = require('express-fileupload')

const Database = require('./Database')
const IdentityRouter = require('../services/identity/IdentityRouter')
const BlockchainRouter = require('../services/blockchain/BlockchainRouter')
const ExchangeRouter = require('../services/exchange/ExchangeRouter')
const InvestmentRouter = require('../services/investment/InvestmentRouter')
const DataroomRouter = require('../services/dataroom/DataroomRouter')
const CustodyRouter = require('../services/custody/CustodyRouter')
const cors = require('cors')

function Server (deps) {
  this.log = deps.logger
  this.port = deps.config.server.PORT
  this.env = deps.config.server.NODE_ENV
  this.database = new Database(deps)
  this.app = express()
}

Server.prototype.init = function () {
  try {
    this.addMiddleware()
    this.addRoutes()
    this.log.info(`API Server started env: ${this.env} on port ${this.port}`)
    return this.app.listen(this.port)
  } catch (e) {
    this.log.error(e)
    throw e
  }
}

Server.prototype.addMiddleware = function () {
  this.app.use(cors())
  this.app.use(helmet())
  this.app.disable('x-powered-by')
  this.app.use(logger('combined'))
  this.app.use(bodyParser.urlencoded({ limit: '150mb', extended: false }))
  this.app.use(bodyParser.json({ limit: '150mb' }))
  this.app.use(
    fileUpload({
      limits: { fileSize: 25 * 1024 * 1024 },
      useTempFiles: false,
      abortOnLimit: true
    })
  )
}

Server.prototype.addRoutes = function () {
  const exchangeRouter = new ExchangeRouter()
  const investmentRouter = new InvestmentRouter()
  const custodyRouter = new CustodyRouter()
  const identityRouter = new IdentityRouter(deps)
  const blockchainRouter = new BlockchainRouter(deps)
  const dataroomRouter = new DataroomRouter(deps)

  this.app.use('/blockchain', blockchainRouter)
  this.app.use('/dataroom', dataroomRouter)
  this.app.use('/custody', custodyRouter)
  this.app.use('/exchange', exchangeRouter)
  this.app.use('/identity', identityRouter)
  this.app.use('/investment', investmentRouter)
  this.app.use('/health-check', (req, res) => res.status(200).send('ok'))
}

module.exports = Server
